import { Command, Flags } from "@oclif/core";
import chalk from "chalk";

import * as core from "../core";

export default class Status extends Command {
  static description = "Show the working migration status.";

  static examples = ["<%= config.bin %> <%= command.id %>"];

  static flags = {
    verbose: Flags.boolean({
      char: "v",
      default: false,
      description: "Verbose output to STDOUT.",
    }),
  };

  static args = [];

  public async run(): Promise<void> {
    const { flags } = await this.parse(Status);
    const isVerbose = flags.verbose;

    if (isVerbose) {
      this.log("Creating API...");
    }
    const api = await core.createApi(this.config.configDir);

    const latestMigrationApplied = api.items.find(core.isMigItemApplied);
    if (isVerbose && latestMigrationApplied) {
      const label = core.labelFromSemVer(latestMigrationApplied.semVer);
      this.log("Latest applied migration found: " + label);
    }

    const latestReleaseTag = api.items.find(core.isMigItemTagged);
    if (isVerbose && latestReleaseTag) {
      const label = core.releaseTagOriginFromReleaseTag(
        api.config,
        latestReleaseTag.tag
      );
      this.log("Latest release tag found: " + label);
    }

    const latestMigrationReleased = api.items.find(
      core.isMigItemTaggedAvailable
    );
    if (isVerbose && latestMigrationReleased) {
      const label = core.labelFromSemVer(latestMigrationReleased.semVer);
      this.log("Latest migration with release tag found: " + label);
    }

    if (isVerbose) {
      this.log("");
    }

    if (latestMigrationApplied) {
      const label = core.labelFromSemVer(latestMigrationApplied.semVer);
      this.log(`Applied migration is ${label}.`);
    } else {
      this.warn("Applied migration is not available.");
    }
    if (latestMigrationReleased) {
      const label = core.labelFromSemVer(latestMigrationReleased.semVer);
      this.log(`Tagged migration is ${label}.`);
    } else {
      this.warn("Tagged migration is not available.");
    }

    this.log("\nChangelog entries:");
    const changelogEntries = api.items.filter(core.isMigItemApplied);
    changelogEntries.forEach((val) => {
      let label = core.labelFromSemVer(val.semVer);
      if (core.isMigItemTagged(val)) {
        label +=
          " @" + core.releaseTagOriginFromReleaseTag(api.config, val.tag);
      }

      switch (val.kind) {
        case core.MigItemKind.__A:
        case core.MigItemKind.T_A:
          if (
            latestMigrationApplied &&
            core.hasEqualSemVerPrecedence(
              latestMigrationApplied.semVer,
              val.semVer
            )
          ) {
            this.log("\t" + chalk.red(label + " ✔"));
          } else {
            this.log("\t" + chalk.red(label));
          }
          break;
        case core.MigItemKind._MA:
        case core.MigItemKind.TMA:
          if (
            latestMigrationApplied &&
            core.hasEqualSemVerPrecedence(
              latestMigrationApplied.semVer,
              val.semVer
            )
          ) {
            this.log("\t" + chalk.green(label + " ✔"));
          } else {
            this.log("\t" + chalk.green(label));
          }
          break;
        default:
          ((_val: never): never => {
            throw new Error(`Cannot log item: ${_val}`);
          })(val);
      }
    });
    if (!changelogEntries.length) {
      this.log("\t(none)");
    }

    this.log("\nMigration scripts:");
    const migrationScripts = api.items.filter(core.isMigItemAvailable);
    migrationScripts.forEach((val) => {
      let label = core.labelFromSemVer(val.semVer);
      if (core.isMigItemTagged(val)) {
        label +=
          " @" + core.releaseTagOriginFromReleaseTag(api.config, val.tag);
      }

      switch (val.kind) {
        case core.MigItemKind._M_:
        case core.MigItemKind.TM_:
          this.log("\t" + chalk.blue(label));
          break;
        case core.MigItemKind._MA:
        case core.MigItemKind.TMA:
          if (
            latestMigrationApplied &&
            core.hasEqualSemVerPrecedence(
              latestMigrationApplied.semVer,
              val.semVer
            )
          ) {
            this.log("\t" + chalk.green(label + " ✔"));
          } else {
            this.log("\t" + chalk.green(label));
          }
          break;
        default:
          ((_val: never): never => {
            throw new Error(`Cannot log item: ${_val}`);
          })(val);
      }
    });
    if (!migrationScripts.length) {
      this.log("\t(none)");
    }

    this.log("\nRelease tags:");
    const releaseTags = api.items.filter(core.isMigItemTagged);
    releaseTags.forEach((val) => {
      const label = core.releaseTagOriginFromReleaseTag(api.config, val.tag);

      switch (val.kind) {
        case core.MigItemKind.T__:
          this.log("\t" + chalk.grey(label));
          break;
        case core.MigItemKind.T_A:
          this.log("\t" + chalk.red(label));
          break;
        case core.MigItemKind.TM_:
          this.log("\t" + chalk.yellow(label));
          break;
        case core.MigItemKind.TMA:
          if (
            latestMigrationApplied &&
            core.hasEqualSemVerPrecedence(
              latestMigrationApplied.semVer,
              val.semVer
            )
          ) {
            this.log("\t" + chalk.green(label + " ✔"));
          } else {
            this.log("\t" + chalk.green(label));
          }
          break;
        default:
          ((_val: never): never => {
            throw new Error(`Cannot log item: ${_val}`);
          })(val);
      }
    });
    if (!releaseTags.length) {
      this.log("\t(none)");
    }

    // svms status
    this.log("\nStatus:");
    const { hasConsistency } = api.items
      .filter(core.isMigItemAvailable)
      .reduce<{ hasAppliedPrev: boolean | null; hasConsistency: boolean }>(
        (acc, val) => {
          if (!acc.hasConsistency) {
            return acc;
          }

          const hasAppliedPrev = core.isMigItemApplied(val);
          if (!acc.hasAppliedPrev) {
            return {
              ...acc,
              hasAppliedPrev,
            };
          }

          return {
            hasAppliedPrev,
            hasConsistency: hasAppliedPrev,
          };
        },
        {
          hasAppliedPrev: null,
          hasConsistency: true,
        }
      );
    if (!hasConsistency) {
      const label = latestMigrationApplied
        ? core.labelFromSemVer(latestMigrationApplied.semVer)
        : core.CheckoutArgument.Head;
      this.warn(
        "Migrations are not applied consistently.\n" +
          `\t(use "svms checkout -f ${label}" to try to fix that)\n`
      );
    }
    if (latestMigrationApplied && latestMigrationReleased) {
      this.log("");
      if (
        !core.hasEqualSemVerPrecedence(
          latestMigrationApplied.semVer,
          latestMigrationReleased.semVer
        )
      ) {
        this.log(
          "Migrations are not in sync with the latest git release tag.\n" +
            `\t(use "svms checkout ${core.CheckoutArgument.Tag}" to migrate to the latest release)\n`
        );
      } else {
        this.log("nothing to do");
      }
    }

    this.exit();
  }
}
