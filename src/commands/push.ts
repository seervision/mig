import { Command, Flags } from "@oclif/core";

import * as core from "../core";

export default class Push extends Command {
  static description =
    "Push a migration script to the changelog without running it.";

  static examples = ["<%= config.bin %> <%= command.id %> 1.0.1"];

  static flags = {
    dry: Flags.boolean({
      char: "d",
      default: false,
      description:
        "Make any changes as a dry-run, so no changes will be applied.",
    }),
    force: Flags.boolean({
      char: "f",
      default: false,
      description: "Force a push (use with care).",
    }),
    verbose: Flags.boolean({
      char: "v",
      default: false,
      description: "Verbose output to STDOUT.",
    }),
  };

  static args = [
    {
      name: "target",
      required: true,
      description: "semver target of migration to push",
    },
  ];

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Push);
    const targetRaw = args.target;
    const dry = flags.dry;
    const force = flags.force;
    const verbose = flags.verbose;

    const target = core.semVerParsed(targetRaw);
    if (!target) {
      this.error("Cannot parse target to semver: " + targetRaw, { exit: 1 });
    }

    if (verbose) {
      this.log("Creating API...");
    }
    const api = await core.createApi(this.config.configDir);

    if (verbose) {
      this.log("Look for items...");
    }
    const items = api.items
      .filter(core.isMigItemMigration)
      .filter(core.isMigItemMatchingSemVer(target))
      .sort(core.orderForReverting);
    if (!items.length) {
      this.error(`No migration found that satisfies target ${targetRaw}.\n`, {
        exit: 1,
      });
    }

    // we might have nothing to do
    if (items.length === 1 && core.isMigItemApplied(items[0])) {
      this.log("nothing to do");
      this.exit(0);
    }

    // require force to indicate a dangerous operation
    if (!force) {
      this.error(
        "Won't push anything to the changelog manually.\n" +
          "  (use -f to force push)\n",
        {
          exit: 1,
        }
      );
    }

    // push
    if (verbose) {
      this.log("Pushing migrations...");
    }
    for (const item of items) {
      if (!dry) {
        await api.push(item.migration);
      }
    }

    if (verbose) {
      this.log("");
    }
    this.log("done");

    this.exit();
  }
}
