import {
  ChangelogConnection,
  changelogConnectionFromConfig,
} from "../resources/changelog-connection";
import { Config, readConfig } from "../resources/config";
import {
  MigrationScriptKind,
  applyMigrationScript,
  createMigrationScript,
  listMigrationScripts,
  revertMigrationScript,
} from "../resources/migration-script";
import { getReleaseTags } from "../resources/release-tag";
import { readPkgSemVer } from "../resources";
import { FilePathAbs } from "../types";
import { MigItem, migItemsFromResources } from "./mig-item";
import {
  isMigrationApplied,
  isMigrationAvailable,
  Migration,
} from "./migration";
import { scriptEnvFromMigration } from "./script-env";
import { SemVer, semVersFromConfig } from "./sem-ver";

export interface Api {
  apply: (migration: Migration) => Promise<void>;
  config: Config;
  create: (
    semVer: SemVer,
    scriptKind: MigrationScriptKind
  ) => Promise<FilePathAbs>;
  hasConfiguration: (semVer: SemVer) => boolean;
  items: MigItem[];
  push: (migration: Migration) => Promise<void>;
  revert: (migration: Migration) => Promise<void>;
}

/**
 *
 * @param configDir
 * @returns
 */
export async function createApi(configDir: FilePathAbs): Promise<Api> {
  const pkgSemVer = await readPkgSemVer();
  const config = await readConfig(configDir);
  const semVers = semVersFromConfig(config);

  // resolve resources
  const changelogConnection = await changelogConnectionFromConfig(
    config,
    pkgSemVer
  );
  const migrations = await changelogConnection.listMigrationsApplied();
  const releaseTags = await getReleaseTags(config);
  const migrationScripts = await listMigrationScripts(config);

  // create interface
  const apply = applyMigration.bind(null, config, changelogConnection);
  const create = createMigrationScript.bind(null, config);
  const hasConfiguration = (semVer: SemVer): boolean =>
    semVers.some((val) => val.major === semVer.major);
  const items = migItemsFromResources(
    semVers,
    releaseTags,
    migrations,
    migrationScripts
  );
  const push = pushMigration.bind(null, changelogConnection);
  const revert = revertMigration.bind(null, config, changelogConnection);

  return {
    apply,
    config,
    create,
    hasConfiguration,
    items,
    push,
    revert,
  };
}

/**
 * Apply a migration in an idempotent manner.
 *
 * @param config
 * @param changelogConnection
 * @param migration
 */
async function applyMigration(
  config: Config,
  changelogConnection: ChangelogConnection,
  migration: Migration
): Promise<void> {
  if (isMigrationApplied(migration)) {
    await changelogConnection.deleteMigrationApplied(migration);
  }
  if (!isMigrationApplied(migration) && isMigrationAvailable(migration)) {
    const env = scriptEnvFromMigration(config, migration);
    await applyMigrationScript(env, migration);
  }
  if (isMigrationAvailable(migration)) {
    await changelogConnection.postMigrationAvailable(migration);
  }
}

/**
 * Revert a migration in an idempotent manner.
 *
 * @param config
 * @param changelogConnection
 * @param migration
 */
async function revertMigration(
  config: Config,
  changelogConnection: ChangelogConnection,
  migration: Migration
): Promise<void> {
  if (isMigrationApplied(migration) && isMigrationAvailable(migration)) {
    const env = scriptEnvFromMigration(config, migration);
    await revertMigrationScript(env, migration);
  }
  if (isMigrationApplied(migration)) {
    await changelogConnection.deleteMigrationApplied(migration);
  }
}

/**
 *
 * @param changelogConnection
 * @param migration
 */
async function pushMigration(
  changelogConnection: ChangelogConnection,
  migration: Migration
): Promise<void> {
  if (isMigrationApplied(migration)) {
    await changelogConnection.deleteMigrationApplied(migration);
  }
  if (isMigrationAvailable(migration)) {
    await changelogConnection.postMigrationAvailable(migration);
  }
}
