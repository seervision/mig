import type { MigrationAvailable } from "../resources";
import type { Sha256Sum } from "../types";
import type { SemVer } from "./sem-ver";

export interface MigrationApplied {
  appliedAt: Date;
  semVer: SemVer;
  sha256sum: Sha256Sum;
}
export type MigrationAppliedAvailable = MigrationApplied & MigrationAvailable;
export type Migration =
  | MigrationApplied
  | MigrationAvailable
  | MigrationAppliedAvailable;

/**
 *
 * @param migration
 * @returns
 */
export function isMigrationAvailable(
  migration: Migration
): migration is MigrationAvailable | MigrationAppliedAvailable {
  return "filePath" in migration;
}

/**
 *
 * @param migration
 * @returns
 */
export function isMigrationApplied(
  migration: Migration
): migration is MigrationApplied | MigrationAppliedAvailable {
  return "appliedAt" in migration;
}
