import cp from "child_process";
import crypto from "crypto";
import fs from "fs";
import fsPromises from "fs/promises";
import path from "path";

import fse from "fs-extra";
import globby from "globby";

import { fileNameFromSemVer, semVerFromFileName } from "../core/file-name";
import type { MigrationAppliedAvailable } from "../core/migration";
import type { ScriptEnv } from "../core/script-env";
import {
  SemVer,
  hasEqualSemVerPrecedence,
  labelFromSemVer,
} from "../core/sem-ver";
import type { FilePathAbs, Sha256Sum } from "../types";
import type { Config } from "./config";
import { dataDir } from "./self";

export enum MigrationScriptKind {
  Node = "js",
  Python = "py",
}

interface MigrationScriptBase<T extends MigrationScriptKind> {
  filePath: FilePathAbs;
  kind: T;
  semVer: SemVer;
  sha256sum: Sha256Sum;
}
type MigrationScriptNode = MigrationScriptBase<MigrationScriptKind.Node>;
type MigrationScriptPython = MigrationScriptBase<MigrationScriptKind.Python>;
type MigrationScript = MigrationScriptNode | MigrationScriptPython;

export type MigrationAvailable = MigrationScript;

/**
 * List migration scripts in arbitrary order.
 *
 * @param migrationScriptsDir
 * @returns
 */
export async function listMigrationScripts(
  config: Config
): Promise<Array<MigrationAvailable>> {
  const migrationScriptsDir = config.migrations.scriptsDir;

  const suffixes = [MigrationScriptKind.Node, MigrationScriptKind.Python].join(
    "|"
  );

  const paths = await globby([
    `${migrationScriptsDir}/*.+(${suffixes})`,
    `!${migrationScriptsDir}/template.+(${suffixes})`,
  ]);
  const scripts = await Promise.all(
    paths.map(async (filePath): Promise<MigrationAvailable> => {
      const kind = migrationScriptKindFromFilePath(filePath);
      const sha256sum = await sha256sumFromFilePath(filePath);
      const fileName = fileNameFromFilePath(filePath);
      const semVer = semVerFromFileName(fileName);

      return {
        filePath,
        kind,
        semVer,
        sha256sum,
      };
    })
  );

  return scripts.reduce<Array<MigrationAvailable>>((acc, val) => {
    if (
      acc.some(({ semVer }) => hasEqualSemVerPrecedence(semVer, val.semVer))
    ) {
      const label = labelFromSemVer(val.semVer);
      throw new Error(
        "Cannot handle duplicated migration scripts for semver: " + label
      );
    }

    acc.push(val);
    return acc;
  }, []);
}

/**
 *
 * @param config
 * @param semVer
 * @param scriptKind
 * @returns
 */
export async function createMigrationScript(
  config: Config,
  semVer: SemVer,
  scriptKind: MigrationScriptKind
): Promise<FilePathAbs> {
  const migrationScriptsDir = config.migrations.scriptsDir;
  const suffix = suffixFromMigrationScriptKind(scriptKind);
  const fileName = fileNameFromSemVer(semVer);

  const basename = fileName + suffix;
  const filePath = path.join(migrationScriptsDir, basename);

  await fse.ensureDir(migrationScriptsDir);

  // try to copy user template
  const filePathTemplateUser = path.join(
    migrationScriptsDir,
    ".template" + suffix
  );
  const resultUser = await copyTemplate(filePathTemplateUser, filePath);
  if (resultUser) {
    return filePath;
  }

  // try to copy default template
  const filePathTemplateDefault = path.join(dataDir(), "template" + suffix);
  const resultDefault = await copyTemplate(filePathTemplateDefault, filePath);
  if (resultDefault) {
    return filePath;
  }

  throw new Error("Cannot resolve template for a migration script");
}

/**
 * Run a single migration script to upgrade.
 *
 * TODO: make this configurable per filetype and workable even with containers
 *
 * @param env
 * @param migration
 */
export async function applyMigrationScript(
  env: ScriptEnv,
  migration: MigrationAvailable
): Promise<void> {
  const options = {
    cwd: path.dirname(migration.filePath),
    env: {
      ...process.env,
      ...env,
    },
  };

  const scriptKind = migrationScriptKindFromFilePath(migration.filePath);
  switch (scriptKind) {
    case MigrationScriptKind.Node: {
      const node = cp.spawn(
        "node",
        ["--unhandled-rejections=strict", migration.filePath, "up"],
        options
      );
      return cmdResultFromCmd(node);
    }
    case MigrationScriptKind.Python: {
      const python = cp.spawn("python3", [migration.filePath, "up"], options);
      return cmdResultFromCmd(python);
    }
    default:
      ((val: never): never => {
        throw new Error("Cannot apply kind of migration: " + val);
      })(scriptKind);
  }
}

/**
 * Run a single migration script to downgrade.
 *
 * TODO: make this configurable per filetype and workable even with containers
 *
 * @param env
 * @param migration
 */
export async function revertMigrationScript(
  env: ScriptEnv,
  migration: MigrationAvailable | MigrationAppliedAvailable
): Promise<void> {
  const options = {
    cwd: path.dirname(migration.filePath),
    env: {
      ...process.env,
      ...env,
    },
  };

  const scriptKind = migrationScriptKindFromFilePath(migration.filePath);
  switch (scriptKind) {
    case MigrationScriptKind.Node: {
      const node = cp.spawn(
        "node",
        ["--unhandled-rejections=strict", migration.filePath, "down"],
        options
      );
      return cmdResultFromCmd(node);
    }
    case MigrationScriptKind.Python: {
      const python = cp.spawn("python3", [migration.filePath, "down"], options);
      return cmdResultFromCmd(python);
    }
    default:
      ((val: never): never => {
        throw new Error("Cannot revert kind of migration: " + val);
      })(scriptKind);
  }
}

/**
 *
 * @param cmd
 * @returns
 */
function cmdResultFromCmd(
  cmd: cp.ChildProcessWithoutNullStreams
): Promise<void> {
  // propagate STDOUT
  cmd.stdout.on("data", (data) => {
    console.log(`${data}`);
  });

  // propagate STDERR
  cmd.stderr.on("data", (data) => {
    console.error(`${data}`);
  });

  // propagate ERRNO
  return new Promise((resolve, reject) => {
    cmd.on("close", (code, _signal) => {
      if (code !== 0) {
        reject(new Error("Migration script raised error"));
      }

      resolve();
    });
  });
}

/**
 *
 * @param filePath
 * @returns
 */
function migrationScriptKindFromFilePath(
  filePath: FilePathAbs
): MigrationScriptKind {
  const basename = path.basename(filePath);
  const suffix = suffixFromBasename(basename);
  switch (suffix) {
    case ".js":
      return MigrationScriptKind.Node;
    case ".py":
      return MigrationScriptKind.Python;
    default:
      throw new Error(
        "Cannot determine migration script kind from suffix: " + suffix
      );
  }
}

/**
 *
 * @param scriptKind
 * @returns
 */
function suffixFromMigrationScriptKind(
  scriptKind: MigrationScriptKind
): string {
  switch (scriptKind) {
    case MigrationScriptKind.Node:
      return ".js";
    case MigrationScriptKind.Python:
      return ".py";
    default:
      ((val: never): never => {
        throw new Error("Cannot resolve suffix for kind: " + val);
      })(scriptKind);
  }
}

/**
 *
 * @param basename
 * @returns
 */
function suffixFromBasename(basename: string): string {
  let suffix = "";
  while (true) {
    const basenameSliced = suffix.length
      ? basename.slice(0, -suffix.length)
      : basename;
    const extname = path.extname(basenameSliced);
    if (!extname) {
      break;
    }
    suffix = extname + suffix;
  }
  return suffix;
}

/**
 *
 * @param filePath
 */
function fileNameFromFilePath(filePath: FilePathAbs): string {
  const basename = path.basename(filePath);
  const suffix = suffixFromBasename(basename);
  return suffix.length ? basename.slice(0, -suffix.length) : basename;
}

/**
 * Compute a sha256 checksum of the file.
 *
 * @param filePath
 * @returns
 */
async function sha256sumFromFilePath(
  filePath: FilePathAbs
): Promise<Sha256Sum> {
  const data = await fsPromises.readFile(filePath);
  return crypto.createHash("sha256").update(data).digest("hex");
}

/**
 *
 * @param filePathTemplate
 * @param filePath
 * @returns
 */
async function copyTemplate(
  filePathTemplate: FilePathAbs,
  filePath: FilePathAbs
): Promise<FilePathAbs | null> {
  let isFileAccessible = false;
  try {
    await fsPromises.access(filePathTemplate, fs.constants.R_OK);
    isFileAccessible = true;
  } catch {}
  if (!isFileAccessible) {
    return null;
  }

  await fsPromises.copyFile(filePathTemplate, filePath);
  return filePath;
}
