import fs from "fs";
import fsPromises from "fs/promises";
import path from "path";

import Ajv from "ajv";
import fse from "fs-extra";

import type { ScriptEnv } from "../core";
import type { FilePathAbs } from "../types";
import schema from "./config-schema.json";
import { dataDir } from "./self";

export enum ChangelogConnectionKind {
  MongoDB = "mongodb",
}

interface ChangelogConnectionConfigBase<
  TKind extends ChangelogConnectionKind,
  TParams extends Record<string, unknown>
> {
  kind: TKind;
  params: TParams;
}
export interface ChangelogConnectionConfigMongoDB
  extends ChangelogConnectionConfigBase<
    ChangelogConnectionKind.MongoDB,
    {
      collection: string;
      database: string;
      host: string;
      port: number;
    }
  > {}
type ChangelogConnectionConfig = ChangelogConnectionConfigMongoDB;

interface Changelog {
  connection: ChangelogConnectionConfig;
}

interface Migrations {
  scriptsDir: FilePathAbs;
}

interface ReleasesBase {
  scriptEnv?: ScriptEnv;
}
export interface ReleasePatch extends ReleasesBase {}
export interface ReleasesMinor extends ReleasesBase {
  [patch: string]: ReleasePatch | ScriptEnv | undefined;
}
export interface ReleasesMajor extends ReleasesBase {
  [minor: string]: ReleasesMinor | ScriptEnv | undefined;
}
export interface Releases extends ReleasesBase {
  [major: string]: ReleasesMajor | ScriptEnv | undefined;
}

interface Repository {
  rootDir: FilePathAbs;
  tagPrefix?: string;
}

export interface Config {
  changelog: Changelog;
  migrations: Migrations;
  releases: Releases;
  repository: Repository;
  version: "1";
}

/**
 * Validation
 */
const ajv = new Ajv({ discriminator: true });
const validate = ajv.compile(schema);

/**
 *
 * @param configDir
 * @returns
 */
export async function readConfig(configDir: FilePathAbs): Promise<Config> {
  const config = await loadAndEnsureConfig(configDir);
  if (!config) {
    throw new Error("Cannot resolve config");
  }

  const valid = validate(config);
  if (!valid && validate.errors && validate.errors.length) {
    const error = validate.errors.shift()!;
    const keyPath = error.instancePath.slice(1).split("/").join(".");
    throw new Error("Invalid config: " + keyPath + " " + error.message);
  }

  return config as unknown as Config;
}

/**
 *
 * @param configDir
 * @returns
 */
async function loadAndEnsureConfig(
  configDir: FilePathAbs
): Promise<any | null> {
  const basename = "config.json";

  // try to load user config
  const filePathUser = path.join(configDir, basename);
  let isFileAccessibleUser = false;
  try {
    await fsPromises.access(filePathUser, fs.constants.R_OK);
    isFileAccessibleUser = true;
  } catch {}
  if (isFileAccessibleUser) {
    return fse.readJSON(filePathUser);
  }

  // try to load default config
  const filePathDefault = path.resolve(path.join(dataDir(), basename));
  let isFileAccessibleDefault = false;
  try {
    await fsPromises.access(filePathDefault, fs.constants.R_OK);
    isFileAccessibleDefault = true;
  } catch {}
  if (!isFileAccessibleDefault) {
    return null;
  }

  await fse.ensureDir(configDir);
  await fsPromises.copyFile(filePathDefault, filePathUser);
  return fse.readJSON(filePathUser);
}
