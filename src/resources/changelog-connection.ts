import { MongoClient } from "mongodb";

import type {
  MigrationApplied,
  MigrationAppliedAvailable,
} from "../core/migration";
import {
  SemVer,
  hasEqualSemVerPrecedence,
  labelFromSemVer,
} from "../core/sem-ver";
import type { Sha256Sum } from "../types";
import {
  ChangelogConnectionConfigMongoDB,
  ChangelogConnectionKind,
  Config,
} from "./config";
import { MigrationAvailable } from "./migration-script";

export interface ChangelogEntry {
  meta: {
    appliedAt: Date;
    semVer: SemVer;
  };
  semVer: SemVer;
  sha256sum: Sha256Sum;
}

export interface ChangelogConnection {
  deleteMigrationApplied: (
    migration: MigrationApplied | MigrationAppliedAvailable
  ) => Promise<void>;
  listMigrationsApplied: () => Promise<MigrationApplied[]>;
  postMigrationAvailable: (migration: MigrationAvailable) => Promise<void>;
}

/**
 *
 * @param config
 * @returns
 */
export async function changelogConnectionFromConfig(
  config: Config,
  pkgSemVer: SemVer
): Promise<ChangelogConnection> {
  // TODO: support other types
  switch (config.changelog.connection.kind) {
    case ChangelogConnectionKind.MongoDB:
      return changelogConnectionFromMongoDBParams(
        config.changelog.connection.params,
        pkgSemVer
      );
    default:
      ((val: never): never => {
        throw new Error("Cannot create changelog connection for kind: " + val);
      })(config.changelog.connection.kind);
  }
}

/**
 *
 * @param params
 * @returns
 */
async function changelogConnectionFromMongoDBParams(
  params: ChangelogConnectionConfigMongoDB["params"],
  pkgSemVer: SemVer
): Promise<ChangelogConnection> {
  const { collection, database, host, port } = params;

  const url = "mongodb://" + host + ":" + port;
  const client = new MongoClient(url);

  await client.connect();
  const db = client.db(database);

  const deleteMigrationApplied = async (
    migration: MigrationApplied | MigrationAppliedAvailable
  ): Promise<void> => {
    await db.collection(collection).deleteOne({
      semVer: migration.semVer,
      sha256sum: migration.sha256sum,
    });
  };

  const listMigrationsApplied = async (): Promise<MigrationApplied[]> => {
    const documents = await db.collection(collection).find().toArray();
    return documents
      .map<MigrationApplied>(migrationAppliedFromChangelogEntry as any)
      .reduce<Array<MigrationApplied>>((acc, val) => {
        if (
          acc.some(({ semVer }) => hasEqualSemVerPrecedence(semVer, val.semVer))
        ) {
          const label = labelFromSemVer(val.semVer);
          throw new Error(
            "Cannot handle duplicated changelog entries for semver: " + label
          );
        }

        acc.push(val);
        return acc;
      }, []);
  };

  const postMigrationAvailable = async (
    migration: MigrationAvailable
  ): Promise<void> => {
    const document = changelogEntryFromMigrationAvailable(pkgSemVer, migration);
    await db.collection(collection).insertOne(document);
  };

  return {
    deleteMigrationApplied,
    listMigrationsApplied,
    postMigrationAvailable,
  };
}

/**
 *
 * @param changelogEntry
 * @returns
 */
function migrationAppliedFromChangelogEntry(
  changelogEntry: ChangelogEntry
): MigrationApplied {
  return {
    appliedAt: changelogEntry.meta.appliedAt,
    semVer: changelogEntry.semVer,
    sha256sum: changelogEntry.sha256sum,
  };
}

/**
 *
 * @param migration
 * @returns
 */
function changelogEntryFromMigrationAvailable(
  pkgSemVer: SemVer,
  migration: MigrationAvailable
): ChangelogEntry {
  return {
    meta: {
      appliedAt: new Date(),
      semVer: pkgSemVer,
    },
    semVer: migration.semVer,
    sha256sum: migration.sha256sum,
  };
}
